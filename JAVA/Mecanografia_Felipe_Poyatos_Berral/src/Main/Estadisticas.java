package Main;

import java.util.Date;

public class Estadisticas {
	
	Date fecha;
	String usuario, tiempo;
	int ppm, fallos;

	public Estadisticas(Date fecha, String usuario, int ppm, String tiempo, int fallos) {
		
		this.fecha = fecha;
		this.usuario = usuario;
		this.ppm = ppm;
		this.tiempo = tiempo;
		this.fallos = fallos;
	}

	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public int getPpm() {
		return ppm;
	}

	public void setPpm(int ppm) {
		this.ppm = ppm;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}

	public int getFallos() {
		return fallos;
	}

	public void setFallos(int fallos) {
		this.fallos = fallos;
	}

	@Override
	public String toString() {
		return "fecha=" + fecha + ", usuario=" + usuario + ", tiempo=" + tiempo + ", ppm=" + ppm
				+ ", fallos=" + fallos;
	}
	
	public String toStringEstadsUser() {
		return usuario;
	}
	
	public Date toStringEstadsFecha() {
		return fecha;
	}
	
	public String toStringficheroStads() {		
		return fecha + ";" + usuario + ";" + ppm + ";" + tiempo + ";" + fallos + "-";
	}
}

