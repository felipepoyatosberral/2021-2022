package Metods;

import javax.swing.JOptionPane;

public class Metods_UI {
	
	public static void preguntarAlSalir() {
		//Funcion para preguntar al salir
		int i = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres salir?", "Cuidado",JOptionPane.YES_NO_OPTION);
		if(i == JOptionPane.YES_OPTION){
			JOptionPane.showMessageDialog(null, "Gracias por usar la aplicación!!", "Gracias", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
	}
}
