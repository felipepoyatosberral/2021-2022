package UI;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class Button extends JButton{
	
	public Button(String texto) {
		setFont(new Font("Tahoma", Font.PLAIN, 15));
		setBackground(Color.WHITE);
		setSize(90, 55);
		setText(texto);
		setFocusPainted(false);
		setFocusable(false);
	}
}
