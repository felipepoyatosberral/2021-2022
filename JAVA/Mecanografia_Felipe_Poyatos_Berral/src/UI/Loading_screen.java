package UI;

import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;


@SuppressWarnings("serial")
public class Loading_screen extends JPanel{
	
	@SuppressWarnings("unused")
	private Main_frame main_frame;
	private JProgressBar loadingbar;
	
	public Loading_screen(Main_frame main_frame){
		this.main_frame = main_frame;
		
		//Caracteristicas del Panel
		setLayout(null);
		
		//Creamos el objeto de la progreesbar y le definimos sus parametros.
		loadingbar = new JProgressBar();
		loadingbar.setStringPainted(true);
		
		loadingbar.setVisible(true);
		loadingbar.setForeground(new Color(0, 0, 0));
		loadingbar.setBackground(new Color(255, 255, 255));
		loadingbar.setBounds(34, 270, 420, 17);
		
		//Creamos el objeto label para la imagen de fondo y le definimos sus parametros
		JLabel label_img = new JLabel("");
		Image img = new ImageIcon("images\\fondo_loading.png").getImage();
		ImageIcon img2=new ImageIcon(img.getScaledInstance(600, 400, Image.SCALE_SMOOTH));
		label_img.setIcon(img2);
		label_img.setBounds(-57, 0, 584, 351);
		
		//Añadimos la progresbar y el label al panel
		add(loadingbar);
		add(label_img);
		
		setVisible(true);
	}

	public JProgressBar getLoadingbar() {
		return loadingbar;
	}

	public void setLoadingbar(JProgressBar loadingbar) {
		this.loadingbar = loadingbar;
	}

	public void progressBar(int x) {
		
		loadingbar.setValue(x);
	}
}
