package Ejercicio6;

public class Modulo{
	
	String nombre_mod;
	int num_horas;
	Profesor profe;
	boolean convalidable;
	
	public Modulo(String nombre_mod, int num_horas, Profesor profe, boolean convalidable) {
		super();
		this.nombre_mod = nombre_mod;
		this.num_horas = num_horas;
		this.profe = profe;
		this.convalidable = convalidable;
	}
	
	public String getNombre_mod() {
		return nombre_mod;
	}
	public void setNombre_mod(String nombre_mod) {
		this.nombre_mod = nombre_mod;
	}
	public int getNum_horas() {
		return num_horas;
	}
	public void setNum_horas(int num_horas) {
		this.num_horas = num_horas;
	}
	public Profesor getProfe() {
		return profe;
	}
	public void setProfe(Profesor profe) {
		this.profe = profe;
	}
	public boolean isConvalidable() {
		return convalidable;
	}
	public void setConvalidable(boolean convalidable) {
		this.convalidable = convalidable;
	}
	
	@Override
	public String toString() {
		return "Modulo [nombre_mod=" + nombre_mod + ", num_horas=" + num_horas + ", profe=" + profe + ", convalidable="
				+ convalidable + "]";
	}
	
	
	
}
