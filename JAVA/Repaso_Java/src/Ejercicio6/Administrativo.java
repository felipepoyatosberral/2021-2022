package Ejercicio6;

public class Administrativo extends Trabajador{
	
	String estudios;
	double antiguedad;

	public Administrativo(String dni, String nombre, String apellido, int salario, double antiguedad, String estudios) {
		super(dni, nombre, apellido, salario);
		this.estudios = estudios;
		this.antiguedad = antiguedad;
		// TODO Auto-generated constructor stub
	}

	public String getStudios() {
		return estudios;
	}

	public void setStudios(String studios) {
		this.estudios = studios;
	}

	public double getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(double antiguedad) {
		this.antiguedad = antiguedad;
	}

	@Override
	public String toString() {
		return "Administrativo [estudios=" + estudios + ", antiguedad=" + antiguedad + ", dni=" + dni + ", nombre="
				+ nombre + ", apellido=" + apellido + ", salario=" + salario + "]";
	}
	
	

}
