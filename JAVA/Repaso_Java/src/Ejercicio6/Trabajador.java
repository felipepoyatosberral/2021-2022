package Ejercicio6;

public class Trabajador extends Persona implements Comparable<Trabajador>{
	
	int salario;
	
	public Trabajador(String dni, String nombre, String apellido, int salario) {
		super(dni, nombre, apellido);
		// TODO Auto-generated constructor stub
		this.salario = salario;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "Trabajador [salario=" + salario + ", dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido
				+ "]";
	}
	
	@Override
	public int compareTo(Trabajador o) {
		if(this.salario>o.salario) {
			return -1;
		}
		if(this.salario<o.salario) {
			return 1;
		}
		return 0;
	}
	

}
