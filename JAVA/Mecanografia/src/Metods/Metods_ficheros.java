package Metods;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;

import Main.Usuarios;
import UI.Login_screen;

public class Metods_ficheros {
	
	public static Usuarios[] leer_fichero(String nombre_fichero) {
		Usuarios[] usuarios = null;
		
		try {
			File file = new File(nombre_fichero);
			FileReader file_read = new FileReader(file);
			@SuppressWarnings("resource")
			BufferedReader buffer_read = new BufferedReader(file_read);
			
			String linea = buffer_read.readLine();
			
			String[] usr = linea.split("-");
			
			usuarios = new Usuarios[usr.length];
			
			for(int i=0; i<usr.length; i++) {
				String[] datos_usr = usr[i].split(";");
				
				usuarios[i] = new Usuarios(datos_usr[0], datos_usr[1], datos_usr[2]);
			}
			
			return usuarios;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	public static Usuarios comprobar_user() {
		
		String user = Login_screen.textField.getText();
		char[] arrayC = Login_screen.passwordField.getPassword();
		String passwd = new String(arrayC);
		
		if(passwd.length()>=4 && passwd.length()<=10) {
			
			Usuarios[] usuarios = leer_fichero("ficheros_txt\\usuarios.txt");
			
			for(int i = 0; i<usuarios.length; i++) {
				if(user.equals(usuarios[i].getNombre()) && passwd.equals(usuarios[i].getPassword())) {
					return usuarios[i];
				}
			}
		}else {
			JOptionPane.showMessageDialog(null, "La contraseña debe tener entre 4 y 10 carácteres", "Contraseña Inválida",0);
		}
		return null;
	}

	public static boolean comprobar_ficheros(int i) {
	
		File fichero_usuarios = new File("ficheros_txt\\usuarios.txt");
		File fichero_textos = new File("ficheros_txt\\textos.txt");
		File fichero_estadisticas = new File("ficheros_txt\\estadisticas.txt");
		
		if(i < 15) {
			if(fichero_usuarios.exists()&&fichero_textos.exists()&&fichero_estadisticas.exists()) {
				return true;
			}
		}
		return false;
	}
}
