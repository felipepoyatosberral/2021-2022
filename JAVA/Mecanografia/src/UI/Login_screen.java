package UI;


import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;

import Metods.Metods;
import Metods.Metods_UI;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;


public class Login_screen extends JPanel{

	private static final long serialVersionUID = 1L;
	
	public static JTextField textField;
	public static JPasswordField passwordField;

	public Login_screen() {
		
		
		setLayout(null);
		
		JLabel lbl_login_tittle = new JLabel("Login");
		lbl_login_tittle.setFont(new Font("Arial Black", Font.BOLD, 25));
		lbl_login_tittle.setBounds(95, 11, 99, 56);
		
		
		JLabel lbl_name = new JLabel("Nombre");
		lbl_name.setBounds(38, 113, 62, 14);
		
		
		textField = new JTextField();
		textField.setBounds(130, 110, 86, 20);
		textField.setColumns(10);
		
		JLabel lbl_passwd = new JLabel("Contraseña");
		lbl_passwd.setBounds(38, 178, 82, 14);
		
		
		JButton login_button = new JButton("Entrar");
		login_button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				boolean x = Metods.entrada_login();
				if(x == true) {
					Metods_UI.abrir_menu();
					setVisible(false);
				}
			}
		});
		login_button.setBounds(95, 262, 89, 23);
		
		
		passwordField= new JPasswordField();
		passwordField.setBounds(130, 175, 86, 20);
		
		add(lbl_login_tittle);
		add(lbl_name);
		add(textField);
		add(lbl_passwd);
		add(login_button);
		add(passwordField);
		
		JCheckBox mostrar_passwd = new JCheckBox("Mostrar");
		mostrar_passwd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mostrar_passwd.isSelected()) {
					passwordField.setEchoChar((char)0);
					mostrar_passwd.setText("Ocultar");
				}else {
					passwordField.setEchoChar('*');
					mostrar_passwd.setText("Mostrar");
				}
			}
		});
		mostrar_passwd.setBounds(130, 202, 97, 23);
		add(mostrar_passwd);
		
		setVisible(true);
		
	}
}
