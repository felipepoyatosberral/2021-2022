package UI;


import javax.swing.JFrame;

import Metods.Metods_UI;

import java.awt.CardLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

public class Main_frame extends JFrame{
	

	private static final long serialVersionUID = 1L;
	
	public Main_frame() {
		
		//Crear panel
		Loading_screen inicio = new Loading_screen();
		Login_screen login = new Login_screen();
		
		login.setVisible(false);

		//Características del JFrame
		setUndecorated(true);
		setVisible(true);
		setBounds(100, 100, 500, 320);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("MecanoPipe");
		getContentPane().setLayout(new CardLayout(0, 0));
		setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_loading.png"));
		
		
		//Añadir paneles al JFrame
		getContentPane().add(inicio);
		
		
		//Abrir login.
		Timer temporizador = new Timer();

		      temporizador.scheduleAtFixedRate(new TimerTask() {
		        int i = 50;

		        @Override
				public void run() {
						
				i--;
							
				if (i < 0) {
					temporizador.cancel();
								
					dispose();			
								
					setSize(290, 380);
					setTitle("MecanoPipe");
					// Mostrar mensaje preguntando si quieres salir
					setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
					addWindowListener(new WindowAdapter() {
						public void windowClosing (WindowEvent e) {
							Metods_UI.preguntarAlSalir();
						}
					});
					
					setLocationRelativeTo(null);
					setResizable(false);
					setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_login.png"));
					setUndecorated(false);
								
								
					getContentPane().remove(inicio);
					getContentPane().add(login);
					login.setVisible(true);						
					setVisible(true);
				}
		      }
		    }, 0, 100);
	}
}
