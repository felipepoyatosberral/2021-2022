package UI;

import java.awt.Color;
import java.awt.Image;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

import Metods.Metods_UI;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Loading_screen extends JPanel{

	private static final long serialVersionUID = 1L;
	
	public Loading_screen() {
		
		//Caracteristicas del Panel
		setVisible(true);
		setLayout(null);
		
		//Creamos el objeto de la progreesbar y le definimos sus parametros.
		JProgressBar loadingbar = new JProgressBar();
		loadingbar.setStringPainted(true);
		
		loadingbar.setVisible(true);
		loadingbar.setForeground(new Color(255, 255, 255));
		loadingbar.setBackground(new Color(192, 192, 192));
		loadingbar.setBounds(34, 270, 420, 17);
		
		//Creamos el objeto label para la imagen de fondo y le definimos sus parametros
		JLabel label_img = new JLabel("");
		Image img = new ImageIcon("images\\fondo_loading.png").getImage();
		ImageIcon img2=new ImageIcon(img.getScaledInstance(600, 400, Image.SCALE_SMOOTH));
		label_img.setIcon(img2);
		label_img.setBounds(-57, 0, 584, 351);
		
		//Llamamos a la funcion para dar la funcionalidad a la progressbar
		Metods_UI.barra_de_progreso(loadingbar);
		
		//Añadimos la progresbar y el label al panel
		add(loadingbar);
		add(label_img);
	}
}
