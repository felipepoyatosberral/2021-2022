package Main;

public class Texto {
	String text_facil, text_dificil;

	public Texto(String text_facil, String text_dificil) {
		super();
		this.text_facil = text_facil;
		this.text_dificil = text_dificil;
	}

	public String getText_facil() {
		return text_facil;
	}

	public void setText_facil(String text_facil) {
		this.text_facil = text_facil;
	}

	public String getText_dificil() {
		return text_dificil;
	}

	public void setText_dificil(String text_dificil) {
		this.text_dificil = text_dificil;
	}

	@Override
	public String toString() {
		return "Texto [text_facil=" + text_facil + ", text_dificil=" + text_dificil + "]";
	}
	
}
