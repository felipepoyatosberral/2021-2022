package Main;

import java.awt.EventQueue;

import UI.Loading_screen;
import UI.Main_frame;

public class Main {

	protected static final Loading_screen inicio = null;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {   
			public void run() {
				try {
					//Crear JFrame del inicio
					new Main_frame();
				} catch (Exception e) {
					System.out.println("ERROR");
				}
			}
		});
	}
}
