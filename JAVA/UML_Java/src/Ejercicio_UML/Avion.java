package Ejercicio_UML;

public class Avion extends Vehiculo implements Comparable<Avion>{

	private byte motores;
	private double velocidad_max;
	private boolean combate;
	
	public Avion(String marca, String color, String num_bastidor, int kilometros, int año_fabricacion, byte motores, double velocidad_max, boolean combate) {
		super(marca, color, num_bastidor, kilometros, año_fabricacion);
		this.motores = motores;
		this.velocidad_max = velocidad_max;
		this.combate = combate;
	}

	public Avion(String marca, String color, String num_bastidor, int kilometros, int año_fabricacion, boolean combate) {
		super(marca, color, num_bastidor, kilometros, año_fabricacion);
		this.combate = combate;
	}

	public byte getMotores() {
		return motores;
	}

	public void setMotores(byte motores) {
		this.motores = motores;
	}

	public double getVelocidad_max() {
		return velocidad_max;
	}

	public void setVelocidad_max(double velocidad_max) {
		this.velocidad_max = velocidad_max;
	}

	public boolean isCombate() {
		return combate;
	}

	public void setCombate(boolean combate) {
		this.combate = combate;
	}

	@Override
	public String toString() {
		return "Avion [motores=" + motores + ", velocidad_max=" + velocidad_max + ", combate=" + combate + ", marca="
				+ marca + ", color=" + color + ", num_bastidor=" + num_bastidor + ", kilometros=" + kilometros
				+ ", año_fabricacion=" + año_fabricacion + "]";
	}
	
	@Override
	public int compareTo(Avion o) {
		if(this.velocidad_max>o.velocidad_max) {
			return -1;
		}
		if(this.velocidad_max<o.velocidad_max) {
			return 1;
		}
		return 0;
	}
	
}
