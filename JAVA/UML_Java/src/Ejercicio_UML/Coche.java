package Ejercicio_UML;

public class Coche extends Vehiculo{
	
	protected boolean electrico;
	private boolean antiguo;

	public Coche(String marca, String color, String num_bastidor, int kilometros, int año_fabricacion, boolean electrico, boolean antiguo) {
		super(marca, color, num_bastidor, kilometros, año_fabricacion);
		this.electrico = electrico;
		this.antiguo = antiguo;
	}

	public boolean getElectrico() {
		return electrico;
	}

	public void getElectrico(boolean electrico) {
		this.electrico = electrico;
	}

	public boolean getAntiguo() {
		return antiguo;
	}

	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	@Override
	public String toString() {
		return "Coche [electrico=" + electrico + ", antiguo=" + antiguo + ", marca=" + marca + ", color=" + color
				+ ", num_bastidor=" + num_bastidor + ", kilometros=" + kilometros + ", año_fabricacion="
				+ año_fabricacion + "]";
	}
	
}
