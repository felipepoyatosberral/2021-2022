package Ejercicio_UML;

public class Barco extends Vehiculo{
	
	private int eslora, calado;
	private Tipo tipo;
	
	public Barco(String marca, String color, String num_bastidor, int kilometros, int año_fabricacion, int eslora, int calado, Tipo tipo) {
		super(marca, color, num_bastidor, kilometros, año_fabricacion);
		this.eslora = eslora;
		this.calado = calado;
		this.tipo = tipo;
	}
	
	public int getEslora() {
		return eslora;
	}
	public void setEslora(int eslora) {
		this.eslora = eslora;
	}
	public int getCalado() {
		return calado;
	}
	public void setCalado(int calado) {
		this.calado = calado;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Barco [eslora=" + eslora + ", calado=" + calado + ", tipo=" + tipo + ", marca=" + marca + ", color="
				+ color + ", num_bastidor=" + num_bastidor + ", kilometros=" + kilometros + ", año_fabricacion="
				+ año_fabricacion + "]";
	}
	
}
