package Ejercicio_UML;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Crear_vehiculos {
	
	static Scanner leerteclado = new Scanner(System.in);
	
	public static ArrayList<Vehiculo> vehiculos = new ArrayList<>();
	public static ArrayList<Avion> aviones = new ArrayList<>();
	public static ArrayList<Coche> coches = new ArrayList<>();
	
	
	static String marca, color, num_bastidor, num_bastidor_sin_truncar;
	static int kilometros, año_fabricacion, eslora, calado, parar, user_option, option_tipo_barco;
	static boolean antiguo, electrico, combate, user_crear_mas;
	static Tipo tipo;
	static byte motores;
	static double velocidad_max;
	
	public static void datos_vehiculo() {
		
		try {
			System.out.print("Introduce la marca del vehiculo: ");
			marca = leerteclado.nextLine();
			
			System.out.print("Introduce el color del vehiculo: ");
			color = leerteclado.nextLine();
		}catch(InputMismatchException e) {
			System.out.println("\nSolo se permiten carácteres en este campo, Vuelve a intentarlo.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}catch (Exception e) {
			System.out.println("\nHa ocurrido un error inesperado, porfavor vuelve a intentarlo.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}
		
		System.out.print("Introduce número de bastidor del vehiculo: ");
		num_bastidor_sin_truncar = leerteclado.nextLine();
		num_bastidor = num_bastidor_max_chars();
		
		try {
			System.out.print("Introduce los kilometros del vehiculo: ");
			kilometros = leerteclado.nextInt();
			if(kilometros>0) {
				System.out.print("Introduce el año de fabricación del vehiculo: ");
				año_fabricacion = leerteclado.nextInt();
				if(año_fabricacion < 1900) {
					System.out.print("\nEl número debe ser mayor a 1900, porfavor intentelo de nuevo.\n");
					leerteclado.nextLine();
					Menu.menu(vehiculos, aviones, coches);
				}
			
			}else {
				System.out.println("\nLos kilometros deben ser mayor que 0, porfavor intentelo de nuevo.\n");
				leerteclado.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
		} catch (InputMismatchException e) {
			System.out.println("\nTiene que introducir un valor numérico, no se permiten caracteres.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.out.println("\nHa ocurrido un error inesperado, porfavor vuelve a intentarlo.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}
	}
	
	public static void crear_barco() {
		datos_vehiculo();
		
		try {
			System.out.print("Introduce el tamaño de la eslora: ");
			eslora = leerteclado.nextInt();
			if(eslora > 0) {
				System.out.print("Introduce el tamaño del calado: ");
				calado = leerteclado.nextInt();
				if(calado < 0) {
					System.out.println("\nEl calado debe ser mayor que 0, porfavor intentelo de nuevo.\n");
					leerteclado.nextLine();
					Menu.menu(vehiculos, aviones, coches);
				}
			}else {
				System.out.println("\nLa eslora debe ser mayor que 0, porfavor intentelo de nuevo.\n");
				leerteclado.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
			System.out.print("Introduce el tipo de barco: \n- Vela = 1 \n- Pesca = 2 \n- Pasajeros = 3 \n");
			System.out.print("Elije: ");
			option_tipo_barco = leerteclado.nextInt();
			if (option_tipo_barco <= 3 && option_tipo_barco > 0) {
				switch (option_tipo_barco) {
				case 1:
					tipo = Tipo.vela;
				break;
				case 2:
					tipo = Tipo.pesca;
				break;
				case 3:
					tipo = Tipo.pasajeros;
				break;
				}
			}else {
				System.out.printf("\nEl número debe estar entre 0 y 2, por favor vuelve a intentarlo.\n");
				leerteclado.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
		} catch (InputMismatchException e) {
			System.out.println("\nIntroduce un número porfavor, no se permiten carácteres.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.out.println("\nHa ocurrido un error inesperado, por favor intentelo de nuevo\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}
		
		Barco b1 = new Barco(marca, color, num_bastidor, kilometros, año_fabricacion, eslora, calado, tipo);
		
		vehiculos.add(b1);
	}

	public static void crear_avion() {
		datos_vehiculo();
		
		try {
			System.out.print("Introduce el número de los motores: ");
			motores = leerteclado.nextByte();
			if(motores > 0 && motores < 127) {
				System.out.print("Introduce la velocidad máxima: ");
				velocidad_max = leerteclado.nextDouble();
				if(velocidad_max <=0) {
					System.out.println("\nLa velocidad máxima debe ser mayor de 0\n");
					leerteclado.nextLine();
					Menu.menu(vehiculos, aviones, coches);
				}
			}else {
				System.out.println("\nEl número debe ser mayor de 0 y menos que 127, porfavor vuelve a intentarlo.\n");
				leerteclado.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
			System.out.print("¿El avión es de combate? true/false: ");
			combate = leerteclado.nextBoolean();
		}catch(InputMismatchException e) {
			System.out.println("\nEl formato de datos de los campos ha de ser el siguiente: ");
			System.out.println(" 1.- Motores ->          Número del 0 al 127. \n 2.- Velocidad máxima -> Un número. \n 3.- Combate ->          true o false.");
			System.out.println("\nPorfavor intentalo de nuevo introduciendo correctamente el formato de dato en cada campo.");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.out.println("\nHa ocurrido un error inesperado, por favor intentelo de nuevo\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}
			
		Avion a1 = new Avion(marca, color, num_bastidor, kilometros, año_fabricacion, motores, velocidad_max, combate);

		vehiculos.add(a1);
		aviones.add(a1);
		
	}

	public static void crear_coche() {
		datos_vehiculo();
		
		try {
			System.out.print("¿El coche es eléctrico? true/false: ");
			electrico = leerteclado.nextBoolean();
			System.out.print("¿El coche es antiguo? true/false: ");
			antiguo = leerteclado.nextBoolean();
		} catch(InputMismatchException e) {
			System.out.println("\nPorfavor el formato de dato de este campo es true o false, intentelo de nuevo introduciendo correctamente el formato de dato de este campo.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		} catch (Exception e) {
			System.out.println("\nHa ocurrido un error inesperado, por favor intentelo de nuevo\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}
		Coche c1 = new Coche(marca, color, num_bastidor, kilometros, año_fabricacion, electrico, antiguo);
		
		vehiculos.add(c1);
		coches.add(c1);
		
	}
	
	public static void ver_vehiculos() {
		for(int i = 0 ; i<vehiculos.size(); i++) {
			System.out.println(vehiculos.get(i).toString());
		}
	}
	
	public static int crear_mas() {
		
		System.out.print("\n¿Desear hacer algo más? true/false: ");
		try {
			user_crear_mas = leerteclado.nextBoolean();
			leerteclado.nextLine();
		}catch(InputMismatchException e){
			System.out.println("\nPorfavor el formato de dato de este campo es true o false, intentelo de nuevo introduciendo correctamente el formato de dato de este campo.\n");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}
		
		if(user_crear_mas==true) {
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}else if (user_crear_mas == false) {
			parar = 1;
		}
		
		return parar;
	}
	
	public static String num_bastidor_max_chars() {
		int max_char=17;
		
		if(num_bastidor_sin_truncar==null) {
			return null;
		}
		return num_bastidor_sin_truncar.length() < max_char ? num_bastidor_sin_truncar : num_bastidor_sin_truncar.substring(0,max_char);
	}
	
}
