package Clientes_Compras;

import java.util.*;
import java.io.*;

public class Principal {

	public static String[] leerCliente(String nombre, ArrayList<Cliente> lista) {
		
			try 
			{
				File            f = new File(nombre);
				FileReader     fr = new FileReader(f);
				BufferedReader br = new BufferedReader(fr);
				String         linea;
				String[] tabla = new String[4];

				linea = br.readLine();
				while (linea != null)
				{
					// Tratar la línea separado con ; 
					tabla = linea.split(";");
					Cliente c = new Cliente(tabla[0], tabla[1], tabla[2], Integer.parseInt(tabla[3]));
					lista.add(c);
					linea = br.readLine();
				}
				
				// Cerrar el fichero
				br.close();
				return tabla;
			} 
			catch (IOException e) 
			{
				System.out.println(e.getMessage());
				return null;
			}

	}
	
	public static String[] leerCompra(String nombre, ArrayList<Cliente> lista) {
		
		try 
		{
			File            f = new File(nombre);
			FileReader     fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String         linea;
			String[] tabla = new String[4];

			linea = br.readLine();
			while (linea != null)
			{
				// Tratar la línea separado con ; 
				tabla = linea.split(";");
				Compra c = new Compra(tabla[0], tabla[2], tabla[3]);
				for(int i=0; i<lista.size(); i++) {
					if(lista.get(i).getClave() == tabla[1]) {
						lista.get(i).getListaCompra().add(c);
					}
				}
				linea = br.readLine();
			}
			
			// Cerrar el fichero
			br.close();
			return tabla;
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
			return null;
		}

}
	
	public static void main(String[] args) {
		
		ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
		
		leerCliente("src\\Clientes_Compras\\clientes.txt", listaCliente);
		leerCompra("src\\\\Clientes_Compras\\\\Compras.txt", listaCliente);
		
		System.out.println(listaCliente);
	}

}
