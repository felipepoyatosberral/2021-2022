package Ficheros;

import java.io.*;

public class Ficheros {

	public static void main(String[] args) {
		
		
		
		try {
			
			File fichero = new File("prueba.txt"),
				 codigo = new File("codigo"),
				 texto = new File(codigo,"texto"),
				 doc = new File(codigo,"doc");
			
			fichero.createNewFile();
			codigo.mkdir();
			texto.mkdir();
			doc.mkdir();
			
		} catch (IOException e) {
			
			System.out.println("e.getMessage()");
			
		}

	}

}