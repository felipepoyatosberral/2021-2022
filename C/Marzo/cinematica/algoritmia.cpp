#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "algoritmia.h"

void datos(struct TMovil nave) {
	nave.pos.x = 0;
    	nave.pos.y = 0;
    	nave.vel.x = 0.2;
    	nave.vel.y = 0.4;
    	nave.acl.x = -0.02;
    	nave.acl.y = -0.03;
}

int reloj(clock_t *start_t, clock_t *end_t, double *inc_t) {
	*start_t = clock ();
    for (;;) {
        /* Time update */
        *end_t = clock ();
        *inc_t = (double) (*end_t - *start_t) / CLOCKS_PER_SEC;
        *start_t = *end_t;
    }
}

void calc(struct TMovil nave, double *inc_t) {
	nave.vel.x += nave.acl.x * *inc_t;
        nave.vel.y += nave.acl.y * *inc_t;
        nave.pos.x += nave.vel.x * *inc_t;
        nave.pos.y += nave.vel.y * *inc_t;
}

int printar(struct TMovil nave) {
	printf ("Pos x: %.2lf\n", nave.pos.x);
        printf ("Pos y: %.2lf\n", nave.pos.y);
        printf ("Vel x: %.2lf\n", nave.vel.x);
        printf ("Vel y: %.2lf\n", nave.vel.y);
        printf ("Acl x: %.2lf\n", nave.acl.x);
        printf ("Acl y: %.2lf\n", nave.acl.y);
        printf ("\n");

}
