#include <stdio.h>
#include <stdlib.h>

struct TCamisa {
    unsigned talla;
    double precio;
};

int main (int argc, char *argv[]) {
    struct TCamisa camisa0;

    camisa0.talla = 40;
    camisa0.precio = 32.90;

    printf ("Talla: %u\n", camisa0.talla);
    printf ("Precio: %.2lf\n", camisa0.precio);

    char *p = (char *) &camisa0;
    for (size_t i=0; i<sizeof (struct TCamisa); i++)
        printf ("%X ", *p++);

    printf ("\n");

    return EXIT_SUCCESS;
}

