#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio_ext.h>

#define MAXPAL 0x100

int main () {

	/* DECLARACIÓN DE VARIABLES */
	unsigned len;
	unsigned npal = 0;
	char buff[MAXPAL];
	char **lista = NULL;
	
	/* INICIALIZACIÓN */

	/* ENTRADA DE DATOS */
	while (strcmp (buff, "fin") != 0) {
		printf("Introduce una palabra: ");
		scanf(" %[^\n]", buff);
		__fpurge(stdin);

		len = strlen(buff) + 1;
		lista = (char **) realloc (lista, (npal + 1) * sizeof(char *));
		lista[npal] = (char*) malloc (len); 
		strcpy (lista[npal], buff);
		npal++;
	}

	/* CÁLCULOS */

	/* SALIDA DE DATOS  */
	printf("\n");
	for (unsigned i=0; i<npal-1; i++)
		printf("%i.- %s\n", i+1, lista[i]);
	printf("\n");

	/* LIBERACIÓN DE MEMORIA */
	for (unsigned i=0; i<npal; i++)
		free (lista[i]);
	free (lista);
}
