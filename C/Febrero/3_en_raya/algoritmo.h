#ifndef __ALGORITMO_H__
#define __ALGORITMO_H__

#include "general.h"

#ifdef __cplusplus
extern "C" {
#endif
void pintar (unsigned t[N][N]);
void datos(unsigned t[N][N]);
void preguntar_datos(int *fila_user, int *col_user);
bool comprobar_linea_diagonal1(unsigned t[N][N], unsigned fila, unsigned jugador);
bool comprobar_linea_diagonal2(unsigned t[N][N], unsigned fila, unsigned jugador);
bool comprobar_linea_horizontal(unsigned t[N][N], unsigned fila, unsigned jugador);
bool comprobar_linea_vertical(unsigned t[N][N], unsigned fila, unsigned jugador);
void jugador_ganador(unsigned jugador);
#ifdef __cplusplus
}
#endif

#endif
	
