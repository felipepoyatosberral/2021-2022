#include <stdio.h>
#include <stdlib.h>

int main () {
    int *p = NULL;

    p = (int *) malloc (4);

    *p = 7;

    printf("%p: %i\n", p, *p);

    free(p);

    return EXIT_SUCCESS;
}
