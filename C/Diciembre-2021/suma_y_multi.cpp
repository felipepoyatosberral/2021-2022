#include <stdio.h>
int main (){
	int num1, num2, num3, num4, sum1, sum2, mult;

	printf("Introduce el primer número: ");
	scanf(" %i", &num1);

	printf("Introduce el segundo número: ");
	scanf(" %i", &num2);

	printf("Introduce el tercer número: ");
	scanf(" %i", &num3);

	printf("Introduce el cuarto número: ");
	scanf(" %i", &num4);

	sum1 = num1+num2;
	sum2 = num3+num4;
	mult = sum1*sum2;

	printf("La multiplicación de %i y %i es: %i \n", sum1, sum2, mult);

	return 0;
}