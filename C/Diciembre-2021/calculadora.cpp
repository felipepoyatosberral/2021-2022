#include <stdio.h>
#include <ctype.h>

int main () {
	int num1=0,num2=0,resultado;
	char op1;

	//El programa te pide dos números y un operando
	printf("Introduce el primer número: ");
	scanf(" %i", &num1);
	
	printf("Introduce el segundo número: ");
	scanf(" %i", &num2);

	printf("Introduce el operando: ");
	scanf(" %c", &op1);

	//El programa compara el valor que hay dentro de op1 para ver que operando hay y saber que operación es la que debe hacer.
	switch(op1) {
		case '*':
			resultado = num1 * num2;
			printf("%i %c %i = %i \n", num1, op1, num2, resultado);
			break;
		case '+':
			resultado = num1 + num2;
			printf("%i %c %i = %i \n", num1, op1, num2, resultado);
			break;
		case '-':
			resultado = num1 - num2;
			printf("%i %c %i = %i \n", num1, op1, num2, resultado);
			break;
		case '/':
			resultado = num1 / num2;
			printf("%i %c %i = %i \n", num1, op1, num2, resultado);
			break;
		default:
			printf("El valor que has introducido no es válido. \n");
	}
}