#include <stdio.h>
#include <stdlib.h>

//Predicado
int es_multiplo_de (int valor, int multi)
{
	return valor % multi == 0;
}
int main(int argc, char const *argv[])
{
	int numero;

	printf("Número: ");
	scanf ("%i", &numero);

	if (numero == 3)
		printf("Has puesto 3.\n");
	
	if (numero % 2 == 0)
		printf("El número que has introducido (%i) es par.\n", numero);

	else
		printf("El número que has introducido (%i) es impar.\n", numero);

	if (numero != 0)
		printf("%i es distinto de 0.\n", numero);

	if (numero == 0) // numero == 0
		printf("%i es igual a 0.\n", numero);

	if (es_multiplo_de(numero,3))
		printf("%i es multiplo de 3.\n", numero);

		return 0;
}