#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <ctype.h>

void par_impar() {
	int num; //Variable para guardar el número del usuario

	//Pedimos el número al usuario y lo guardamos en la dirección de memoria de num
	printf("Introduce un número: ");
	scanf("%i", &num);

	//Hacemos un if y le decimos si num resto de 2 es 0 el número es par
	if (num % 2 == 0) 
	printf("El número es par \n \n");
	
	//Si no, el número es impar
	else
	printf("El número es impar \n \n");
}

void install() {
	char opcion; //Variable para guardar la opcion del usuario (s/n)

	__fpurge(stdin); //Purgamos el stdin
	
	//Preguntamos al usuario su quiere instalar algo y guardamos la respuesta en la dirección de memoria de opcion
	printf("¿Quieres instalar algo al usuario? s/n \n");
	scanf(" %c", &opcion);

	//Si la opcion es s imprimimos pues lo instalo
	if (opcion == 's')
	printf("Pues lo instalo \n \n");

	//Si la opcion es n imprimimos pues no lo instalo
	if (opcion == 'n')
	printf("Pues no lo instalo \n \n");
}

void install_tolower() {
	char opcion; //Variable para guardar la opcion del usuario (s/n)

        __fpurge(stdin); //Purgamos el stdin

	//Preguntamos al usuario su quiere instalar algo y guardamos la respuesta en la dirección de memoria de opcion
        printf("¿Quieres instalar algo al usuario? s/n \n");
        scanf(" %c", &opcion);
	
	//Convertimos lo que contiene la variable opcion en minúsculas
	opcion = tolower(opcion);

	//Si la opcion es s imprimimos pues lo instalo
        if (opcion == 's')
        printf("Pues lo instalo \n \n");

	//Si la opcion es n imprimimos pues no lo instalo
        if (opcion == 'n')
        printf("Pues no lo instalo \n \n");
}

void diagnosis() {
	char tos; //Variable para guardar si el usuario tose
	char maulla; //Variable para guardar si el usuario maulla

	//Preguntamos al usuario si tiene tos y guardamos la respuesta en la direccion de memoria de tos
	printf("¿Tienes tos? s/n \n");
	scanf(" %c", &tos);
	
	//Preguntamos al usuario si maulla y guardamos la respuesta en la direccion de memoria de maulla
	printf("¿Maullas? s/n \n");
	scanf(" %c", &maulla);

	//Convertimos el contenido de las dos variables en minusculas
	tos = tolower(tos);
	maulla = tolower(maulla);

	//Si el usuario no tose ni maulla esta sano
	if (tos == 'n' && maulla == 'n')
	printf("El usuario está sano \n \n");

	//Si el usuario tose pero no maulla tiene tos ferina
	if (tos == 's' && maulla == 'n')
	printf("El usuario tiene tos ferina \n \n");

	//Si el usuario no tose pero maulla es un gato
	if (tos == 'n' && maulla == 's')
	printf("El usuario es un gato \n \n");

	//Si el usuario tose y maulla tiene tos felina
	if (tos == 's' && maulla == 's')
	printf("El usuario tiene tos felina \n \n");
}

void color() {
	char projo; //Variable para guardar la opcion de rojo
        char pverde, pazul; //Variables para guardar la opcion de verde y azul

	bool rojo; //Varible booleana que utilizaremos en el if 
	bool verde, azul; //Variables booleanas que utilizaremos en el if
	
	__fpurge(stdin); //Purgamos el stdin

	//Le decimos al usuario que piense en un color
        printf("Piensa un color \n");

	//Le preguntamos al usuario si su color contiene rojo, verde o azul y guardamos cada opcion en sus respectivas variables
        printf("¿Tu color tiene rojo? s/n \n");
        scanf(" %c", &projo);

        printf("¿Tu color tiene verde? s/n \n");
        scanf(" %c", &pverde);

        printf("¿Tu color tiene azul? s/n \n");
        scanf(" %c", &pazul);

	//Si la opcion de rojo (projo) es s rojo vale true y si es n rojo vale false y así con los demas colores
	if (projo == 's')
	rojo = "true";

	if (projo == 'n')
	rojo = "false";

	if (pverde == 's')
	verde = "true";

	if (pverde == 'n')
        verde = "false";
	
	if (pazul == 's')
	azul = "true";

	if (pazul == 'n')
        azul = "false";
	

	//Si el color no tiene rojo, ni verde, ni azul el color es negro
	if (rojo == false && verde == false && azul == false)
	printf("Tu color es el negro \n \n");

	//Si el color tiene rojo, verde y azul el color es blanco
	if (rojo == true && verde == true && azul == true)
	printf("Tu color es el blanco \n \n");

	//Si el color tiene rojo y verde pero no azul el color es amarillo
	if (rojo == true && verde == true && azul == false)
	printf("Tu color es el amarillo \n \n");

	//Si el color tiene verde y azul pero no rojo el color es cian
	if (rojo == false && verde == true && azul == true)
	printf("Tu color es el cian \n \n");
	
	//Si el color tiene rojo y azul pero no verde el color es morado
	if (rojo == true && verde == false && azul == true)
	printf("Tu color es el morado \n \n");
}

void entorno() {
	const float E = 0.001; //Variable que utilizamos para calcular el entorno
	float num; //Variable donde guardamos el número del usuario

	//Pedimos al usuario un número y lo guardamos en la dirección de memoria de num
	printf("Introduce un número \n");
	scanf(" %f", &num);
	
	//Si el número es mayor que 2.999 y menor que 3.001 el número se encuentra dentro del entorno 
	if (num > 3-E && num < 3+E)
	printf("El número que has introducido se encuentra dentro del entorno \n \n");
}

int main(){

	system("clear");

	par_impar();
	install();
	install_tolower();
	diagnosis();
	color();
	entorno();

	return EXIT_SUCCESS;

}
