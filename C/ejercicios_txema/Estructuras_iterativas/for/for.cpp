
#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>


void num_naturales() {
	int num;

	//Hacemos un for que se ejecute 10 veces, por cada vez que se ejecute imprimira un número que aumentará por cada for
	for (num=1; num<=10; num++){
	printf(" %i \n", num);
	}
}

void asteriscos() {
	int i;
	
	//Hacemos un for que se ejecute 10 veces y por cada vez que se ejecute imprime un *
	for (i=1; i<=10; i++)
	printf(" *");
}

void caracteres_user() {
	char caracter; //Variable para guardar el caracter que imprime el usuario
	int i; //Variable para la duracion del for
	
	//Pedimos un caracter al usuario y lo guardamos en la direccion de memoria de caracter
	printf("Introduce un caracter: ");
	scanf(" %c", &caracter);
	
	//Hacemos un for que se ejecute 10 veces y cada vez que se ejecute imprima el caracter que ha elejido el usuario
	for (i=1; i<=10; i++)
	printf(" %c", caracter);
}

void caracteres_user_v2() {
	char caracter; //Variable para guardar el caracter que imprime el usuario
	int i; //Variable para la duracion del for
	int veces; //Variable para determinar las veces que se imprime el caracter

	//Pedimos un caracter al usuario y lo guardamos en la direccion de memoria de caracter
	printf("Introduce un caracter: ");
        scanf(" %c", &caracter);
	
	//Pedimos al usuario las veces que quiere que se repita el caracter y lo guardamos en la direccion de memoria de veces
	printf("Introduce las veces que quieres que se repita: ");
        scanf(" %i", &veces);

	//Hacemos un for que se ejecute x veces (depende de la variable veces) y cada vez que se ejecute imprimimos el caracter
	for (i=1; i<=veces; i++)
        printf(" %c", caracter);
}

int media_num() {
	int num; //Variable para guardar el número que le pedimos al usuario
	int i; //Variable para determinar la duracion del for
	int total=0; //Variable que utilizamos para hacer la suma de todos los números

	//Hacemos un for que se ejecute 10 veces, por cada vez que se ejecute le pide un número al usuario, lo guarda en la direccion de memoria de num y hacemos la suma de todos los números con la variable suma
	for (i=1; i<=10; i++){
	printf("Introduce un número: ");
        scanf(" %i", &num);
	total = total + num;
	}

	return total; //Hacemos que la funcion devuelva lo que vale la variable total (suma de todos los números introducidos)
}

int main(){
	int media; //Variable que usaremos para hacer la media de los 10 números
	
	system("clear");
	
	num_naturales();
	printf(" \n");
	asteriscos();
	printf(" \n");
	caracteres_user();
	printf(" \n \n");
	caracteres_user_v2();
	printf(" \n \n");
	
	int total = media_num(); //Total vale lo mismo que lo que devuelve la funcion media_num

	media = (total/10); //Media vale total entre 10 que es el número total de números que hay
	printf("La media de los números que has introducido es: %i \n", media); //Imprimimos la media de los números

	return EXIT_SUCCESS;

}

