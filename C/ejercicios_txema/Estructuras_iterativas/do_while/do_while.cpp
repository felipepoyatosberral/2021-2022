#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

void num_1_10() {
	int num; //Varible para guardar el número que introduce el usuario
	
	//Hacemos un do que le pida al usuario un número del 1 al 10 y lo guarde en la direccion de memoria de num	
	do {
	printf("Introduce un número del 1 al 10: ");
	scanf(" %i", &num);
	}
	
	//Hacemos un while que si el número esta entre 1 y 10 imprima el número si no se vuelva a ejecutar el do
	while (num < 1 && num > 10);
	printf("El número que ha introducido es: %i \n \n", num);
	
}

void num_positivos() {
	int num; //Variable para guardar el número que le pedimos al usuario
	int i=0; //Variable para calcula el numero de numeros que ha introducido el usuario, este incrementará cada veaz que se ejecute el do
	int total=0; //variable para la suma de todos los números que introduce el usuario
	int media; //Variable para hacer la media de los números

	//Hacemos un do que purga el stdin, pide un número al usuario, lo guarda en la direccion de memoria de num, suma todos los números con la variable suma y aumenta i en 1 cada vez que se ejecuta
	do {
	__fpurge(stdin);
	printf("Introduce un número positivo: ");
	scanf(" %i", &num);
	total = total + num;
        i++;
	}

	//Si el número es 0 o menos que 0 se calcula la media de todos los números introducidos y se imprime la misma
	while (num > 0 || num != 0);
	media = total / i; 
	printf("La media de los números que has introducido es: %i \n", media);
}

int main(){
	
	system("clear");
	
	num_1_10();
	num_positivos();

	return EXIT_SUCCESS;

}

