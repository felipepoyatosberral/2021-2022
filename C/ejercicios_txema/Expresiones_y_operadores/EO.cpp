
#include <stdio.h>
#include <stdlib.h>

#define MAX 10

void size_array() {
	int array[MAX]; //Creamos un array de tipo int
	int size = sizeof(array); //Creamos una variable en la que guardamos el tamaño que ocupa el array en bytes gracias al sizeof(array)
	
	printf("El array ocupa %i bytes \n \n", size); //Imprimimos la variable size que contiene los bytes que ocupa el array
}

void amount_elements(){
	int array[MAX]; //Creamos un array de tipo int
	int elementos = sizeof(array)/4; //Creamos una variable (elementos) donde guardamos el tamaño del array entre lo que ocupa cada elemento del array, en este caso 4 bytes ya que es de tipo int, y nos da el número de elementos que tiene ese array

	printf("El array tiene %i elementos \n \n", elementos); //Imprimimos la variable elementos que contiene el número de elementos que tiene el array
}

void long_int() {
	//Creamos tres variables, una de tipo int, otra de tipo long int y otra de tipo long long int.
	int a;
	long int b;
	long long int c;
	//Creamos tres variables con el tamaño de cada una de las anteriores.
	int size_a = sizeof(a);
	int size_b = sizeof(b);
	int size_c = sizeof(c);

	//Imprimimos el tamaño de las tres variables
	printf("%i \n", size_a);
	printf("%i \n", size_b);
	printf("%i \n", size_c);

}

int main(){
	
	system("clear");

	size_array();
	amount_elements();
	long_int();
	
	
	return EXIT_SUCCESS;

}

