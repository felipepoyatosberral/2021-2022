
#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <math.h>

#define MAX 30 

void array_entero() {
	int array[MAX]; //Creación del array
	int i;	//Variable para determinar cuanto dura el for
	int num=1; //Variable para meter los números en el array

	//for para meter los 10 números enteros en el array
	for (i=1; i<=10; i++) {
	array[i]=num;
	num++;
	}

	//for para imprimir los 10 números del array
	for (i=1; i<=10; i++) {
	printf("%i \n", array[i]);
	}
}

void array_entero_v2() {
        int array[MAX]; //Creación del array
        int i;  //Variable para determinar cuanto dura el for
        int num=1; //Variable para meter los números en el array

        //for para meter los 10 números enteros en el array
        for (i=1; i<=10; i++) {
        array[i]=num;
        array[i] = array[i] + array[i-1]; //A cada posicion del array le sumamos el valor de la anterior posicion
	num++;
        }

        //for para imprimir los 10 números del array
        for (i=1; i<=10; i++) {
        printf("%i \n", array[i]);
        }
}

void array_par() {
	int array[MAX]; //Creacion array
	int i; //Variable para la duracion del for
	int num=2; //Variable que usamos para meter los números pares en el array

	//Hacemos un for que se ejecute 10 veces y meta en cada posicion del array num y que sume a num 2 en cada ejecucion
	for (i=1; i<=10; i++) {
	array[i]=num;
	num = num + 2;
	}

	//for para imprimir los 10 números del array
	for (i=1; i<=10; i++){
	printf(" %i \n", array[i]);
	}	
}

void array_impar() {
	int array[MAX]; //Creacion array
        int i; //Variable para la duracion del for
        int num=1; //Variable que usamos para meter los números impares en el array

        //Hacemos un for que se ejecute 10 veces y meta en cada posicion del array num y que sume a num 2 en cada ejecucion
        for (i=1; i<=10; i++) {
        array[i]=num;
        num = num + 2;
        }

        //for para imprimir los 10 números del array
        for (i=1; i<=10; i++){
        printf(" %i \n", array[i]);
        }

}

void array_filas() {
	int A[10][10]; //Creacion array de 10 filas y 10 caracteres cada fila
	int num=0; //Variable que usaremos para introducir los números en el array

	//for que se ejecuta 10 veces, cada vez que se ejecuta cambia la fila donde introducimos los numeros en el array
	for (int i=0; i<10; i++){
		//for que se ejecuta 10 veces y por cada vez va metiendo un número en el array y lo va imprimiendo
		for (int x=1; x<=10; x++){
			A[i][x]=num;
			printf(" %i\n", A[i][x]);
		}
	num++;
	}

}

void array_pow() {
	int A[3][10]; //Array de 3 filas y 10 caracteres cada fila
	int num=0; //Variable que usaremos para introducir números al array
	
	//for que se ejecuta 3 veces, ya que solo tenemos tres filas
        for (int i=1; i<=3; i++) {        
		//for que se ejecuta 10 veces para introducir 10 números en cada una de las filas
		for (int x=0; x<10; x++){
                        A[1][x]=num;
			A[2][x]=pow(num, 2); //Cada vez que se ejecuta introducimos numero elevado a 2 dentro de la segunda fila
			A[3][x]=pow(num, 3); //Cada vez que se ejecuta introducimos numero elevado a 3 dentro de la tercera fila
                        printf(" %i\n", A[i][x]);
                	
			num++;
		}
	}
        
}

int main(){
	
	system("clear");

	array_entero();
	printf("\n");
	array_entero_v2();
	printf("\n");
	array_par();
	printf(" \n");
	array_impar();
	printf(" \n");
	array_filas();
	printf(" \n");
	array_pow();
	return EXIT_SUCCESS;

}

