
#ifndef PEON_H
#define PEON_H

#include <string>
#include "pieza.h"

using namespace std;

class peon: public Pieza::Pieza
{

protected:

	string name;

public:
    
    peon(string _color, string _name);
    virtual void mover(Tablero _tablero);
    void nombre();
};

#endif // PEON_H
