#include "pieza.h"

using namespace std;

Pieza::Pieza(string _color) {
    this->color = _color;
}

void Pieza::setColor(string value) {
	color = value;
}

string Pieza::getColor() {
	return color;
}
